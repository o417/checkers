/**
 * This functions used to log to the console arrays and objects
 */
function printObject(object) {
  console.log('{');
  Object.entries(object).forEach(([k, v]) => {
    console.log(`${k}: ${v.toString()}`)
  });
  console.log('},');
}

function printArray(arr, elementPrinter) {
  console.log('[');
  arr.forEach(element => elementPrinter(element));
  console.log(']');
}

function printPiecesArray(arr) {
  printArray(arr, element => console.log(element.toString() + ','))
}

function printEatablePositions(arr) {
  printArray(arr, element => printObject(element))
}