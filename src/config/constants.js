const WHITE_PLAYER = 1;
const BLACK_PLAYER = -1;
const EMPTY_CELL = 0;

const playersNames = {
  [WHITE_PLAYER]: 'White',
  [BLACK_PLAYER]: 'Black',
}

const BOARD_SIZE = 8;
const NUM_PLAYER_PIECES_ROWS = 3;