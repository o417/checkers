function modalOpen(blackPiecesCounter) {
    const winner = blackPiecesCounter === 0 ? `${playersNames[WHITE_PLAYER]}` : `${playersNames[BLACK_PLAYER]}`;
    const loser = blackPiecesCounter !== 0 ? `${playersNames[WHITE_PLAYER]}` : `${playersNames[BLACK_PLAYER]}`;

    document.getElementById("winner").innerHTML = winner;
    document.getElementById("loser").innerHTML = loser;

    document.getElementById("modal-reason-text").innerText = `Reason: all of ${loser}'s pieces were eaten.`;

    document.getElementById("modal-winner-text").innerText = " won the game !!";
    document.getElementById("modal-loser-text").innerText = ", would you take your revenge ???"

    modal.classList.add("effect");
}

function modalOpenWhenThereAreNoMovesToMake(loserPlayer = currentPlayer) {
    const winner = playersNames[reversePlayer(loserPlayer)];
    const loser = playersNames[loserPlayer];

    document.getElementById("winner").innerHTML = winner;
    document.getElementById("loser").innerHTML = loser;

    document.getElementById("modal-reason-text").innerText = `Reason: all of ${loser}'s pieces are blocked from moving.`;

    document.getElementById("modal-winner-text").innerText = " won the game !!";
    document.getElementById("modal-loser-text").innerText = ", would you take your revenge ???"

    modal.classList.add("effect");
}

function modalClose() {
    modal.classList.remove("effect");
}

function runGameAndModalClose() {
    runGame();
    modalClose();
}
