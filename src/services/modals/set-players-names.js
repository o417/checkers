function setPlayersNamesModalClose() {

    // Set players names.
    playersNames[WHITE_PLAYER] = document.querySelector("#set-players-names-modal [name='white-player-name']").value;
    playersNames[BLACK_PLAYER] = document.querySelector("#set-players-names-modal [name='black-player-name']").value;

    // Display initial new name.
    displayCurrentPlayerName();

    // Close modal.
    document.getElementById("set-players-names-modal").classList.remove("effect");
}

(function setPlayersNamesModalDefaultValue() {

    // Set players default names.
    playersNames[WHITE_PLAYER] = document.querySelector("#set-players-names-modal [name='white-player-name']").value = playersNames[WHITE_PLAYER];
    playersNames[BLACK_PLAYER] = document.querySelector("#set-players-names-modal [name='black-player-name']").value = playersNames[BLACK_PLAYER];
})();
