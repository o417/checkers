/**
 * This function build a piece object
 * Its create the html piece div
 * Every cell is a piece although it can be empty
 * @param row
 * @param column
 */
function buildCell(row, column) {
  let cell = document.createElement("div");

  // set row and colum in the case
  cell.setAttribute("row", row);
  cell.setAttribute("column", column);
  cell.setAttribute("data-position", row + "-" + column);

  cell.setAttribute("class", "occupied " + getCellInnerPiece(row, column));
  cell.setAttribute("king", board[row][column].isKing);

  //add event listener to each piece
  cell.addEventListener("click", doMove);

  return cell;
}

/**
 * This function check if thier is a piece in the cell and return it
 * @param row
 * @param column
 */
function getCellInnerPiece(row, column) {
  if (board[row][column].player === WHITE_PLAYER) {
    occupied = "whitePiece";
  } else if (board[row][column].player === BLACK_PLAYER) {
    occupied = "blackPiece";
  } else {
    occupied = "empty";
  }
  return occupied;
}
