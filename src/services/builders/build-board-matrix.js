function buildBoardMatrix(n, playerRows) {
    const numPlayerRows = playerRows >= n / 2 ? n / 2 - 1 : playerRows
    let board = [];
    for (let i = 0; i < n; i++) {
        board[i] = [];
        for (let j = 0; j < n; j++) {

            if (i < numPlayerRows) {
                board[i][j] = new Piece(i, j, isKing = false, getCellValue(i, j, BLACK_PLAYER));
            }
            else if (i > n - numPlayerRows - 1) {
                board[i][j] = new Piece(i, j, isKing = false, getCellValue(i, j, WHITE_PLAYER));
            }
            else {
                board[i][j] = new Piece(i, j, isKing = false, EMPTY_CELL);
            }
        }
    }

    return board;
}

function getCellValue(row, columnm, player) {
    let value;
    if (row % 2 == 0) {
        if (columnm % 2 == 0) {
            value = EMPTY_CELL;
        } else {
            value = player;
        }
    } else {
        if (columnm % 2 == 0) {
            value = player;
        } else {
            value = EMPTY_CELL;
        }
    }

    return value;
}
