let isClickedOutsideTheSelectedPiece = false;

/**
 * This function builds and display the board
 * Its go over the board and create all the html needed objects
 * according to the board matrix data
 * @param {Array} board global matrix
 */
function buildBoard() {
  game.innerHTML = "";
  let blackPiecesCounter = 0;
  let whitePiecesCounter = 0;

  for (let i = 0; i < board.length; i++) {
    const element = board[i];
    let row = document.createElement("div"); // create div for each row
    row.setAttribute("class", "row");
    row.style.height = 100 / BOARD_SIZE + "%";

    for (let j = 0; j < element.length; j++) {
      let col = document.createElement("div"); // create div for each case

      col.appendChild(buildCell(i, j));
      col.setAttribute("class", "column " + getCaseType(i, j));
      col.style.width = 100 / BOARD_SIZE + "%";
      row.appendChild(col);

      if (board[i][j].player === BLACK_PLAYER) {
        blackPiecesCounter++;
      } else if (board[i][j].player === WHITE_PLAYER) {
        whitePiecesCounter++;
      }
    }
    game.appendChild(row);
  }

  markTheCurrentSelectedPiece();

  setAllowedPositionsToBeClicked();

  markOnlyEatingPaths();

  displayPiecesCounter(blackPiecesCounter, whitePiecesCounter);

  if (blackPiecesCounter === 0 || whitePiecesCounter === 0) {
    modalOpen(blackPiecesCounter);
    return;
  }

  if (!isCurrentPlayerAbleToMakeAnyRegularMoveOrEatingMove()) {
    modalOpenWhenThereAreNoMovesToMake(currentPlayer);
    return;
  }
}

function getCaseType(row, column) {
  let caseType = "";

  if (row % 2 === 0) {
    if (column % 2 === 0) {
      caseType = "Whitecase";
    } else {
      caseType = "blackCase";
    }
  }
  else {
    if (column % 2 !== 0) {
      caseType = "Whitecase";
    } else {
      caseType = "blackCase";
    }
  }

  return caseType;
}

function setAllowedPositionsToBeClicked() {
  allowedPositionsToBeClicked = [];
  for (let rowIndex = 0; rowIndex < board.length; rowIndex++) {
    for (let colIndex = 0; colIndex < board[rowIndex].length; colIndex++) {
      const element = board[rowIndex][colIndex];

      // Enable "current player" cells and `EMPTY_CELL`s.
      if (element.player !== reversePlayer(currentPlayer)) { allowedPositionsToBeClicked.push(element); }
    }
  }
}

function markTheCurrentSelectedPiece() {
  let lastSelectedPiece = pieceToMove;
  if (isClickedOutsideTheSelectedPiece) {
    lastSelectedPiece = undefined;
  }

  const currentPieceElementBeingSelected = document.querySelector(`[row='${lastSelectedPiece?.row}'][column='${lastSelectedPiece?.column}']`);
  currentPieceElementBeingSelected?.setAttribute('selected', true);
}

/**
 * Scans for all the pieces owned by the current player, and check if there are
 * possible eating paths. In case there are, then mark them.
 */
function markOnlyEatingPaths() {
  let totalEatingPaths = [];
  for (let rowIndex = 0; rowIndex < board.length; rowIndex++) {
    for (let colIndex = 0; colIndex < board[rowIndex].length; colIndex++) {
      const element = board[rowIndex][colIndex];
      if (element.player !== currentPlayer) { continue; }
      if (!findEatablePiece(element)) { continue; }
      totalEatingPaths = totalEatingPaths.concat(eatablePiecesPosititions.map(
        (newPositionAndEatablePosition) => {
          let newPos = newPositionAndEatablePosition.newPosition;
          newPos.player = EMPTY_CELL;
          newPos.isKing = false;
          return newPos;
        }
      ));
      totalEatingPaths.push(element);
    }
  }

  totalEatingPaths.forEach(element => {
    markAsEatingPath(element);
  });

  if (totalEatingPaths.length > 0) { allowedPositionsToBeClicked = totalEatingPaths; }
}

function markAsEatingPath(piece) {
  const pieceHtmlElement = document.querySelector(`[row='${piece.row}'][column='${piece.column}']`);
  const caseToMark = pieceHtmlElement.parentNode;
  caseToMark.classList.add("readyToEatCase");
}

function isCurrentPlayerAbleToMakeAnyRegularMoveOrEatingMove() {

  /*
   * Retrieve only the pieces that are owned by the current player, and allowed
   * to be clicked.
   */
  const currentPlayerPiecesAllowedToBeClicked = allowedPositionsToBeClicked.filter(
    (element) => { return element.player === currentPlayer; }
  );

  console.log("currentPlayerPiecesAllowedToBeClicked", currentPlayerPiecesAllowedToBeClicked); // TODO: Debug print.

  /*
   * If any of the pieces is able to make either a move of "regular move" or
   * an "eating move", then return `true`.
   */
  if (currentPlayerPiecesAllowedToBeClicked.some(element => {
    return findEatablePiece(element, colorThePosition = false) ||
      findPossibleNewPosition(element, colorThePosition = false)
  })) {
    possibleNewPositions = [];
    eatablePiecesPosititions = [];
    return true;
  }

  // Otherwise, return `false`.
  return false;
}