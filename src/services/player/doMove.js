/**
 * @type {Piece} The piece that is about to be moved.
 */
let pieceToMove;
let allowedPositionsToBeClicked = [];

/**
 * This function is the main function of the game
 * When clicking a cell on the board this function is called
 * @param {Event} e click cell event
 */
function doMove(e) {
  console.log('---------------------------------------------------');
  console.log(`${playersNames[currentPlayer]} doing move ${moveCounter[currentPlayer]}`);

  let cell = e.target;
  const row = parseInt(cell.getAttribute("row"));
  const column = parseInt(cell.getAttribute("column"));
  let clickedPosition = board[row][column];

  // If the `clickedPosition` is not contained within `allowedPositionsToBeClicked` then return.
  if (!allowedPositionsToBeClicked.some((element) => {
    return element.compare(clickedPosition)
  })) {
    return;
  }

  // `pieceToMove` is only updated when we click on a piece owned by the current player.
  if (clickedPosition.player === currentPlayer) {
    pieceToMove = board[row][column];
    isClickedOutsideTheSelectedPiece = false;
    buildBoard();
  } else {
    isClickedOutsideTheSelectedPiece = true;
  }

  console.log('Clicked Position' + clickedPosition.toString());

  if (eatablePiecesPosititions.length > 0) {
    eatPiece(clickedPosition);
  }
  else {
    if (possibleNewPositions.length > 0) {
      movePiece(clickedPosition);
    }
  }

  if (currentPlayer === board[row][column].player) {
    if (!findEatablePiece(clickedPosition)) {
      findPossibleNewPosition(clickedPosition);
    }
  } else {
    possibleNewPositions = [];
  }

  console.log('---------------------------------------------------');
}
