/**
 * This function is called if possibleNewPositions list size is bigger than 0
 * If the clickedPosition is eaual to one of the optiona regulr moves new positions
 * 
 * The function call to moveThePiece() function and move the player piece
 * @param {Piece} clickedPosition cell event
 */
function movePiece(clickedPosition) {
  let find = false;
  let newPosition = null;

  // check if the case where the player play the selected piece can move on
  possibleNewPositions.forEach((element) => {
    if (element.compare(clickedPosition)) {
      moveCounter[currentPlayer]++;
      find = true;
      newPosition = element;
      console.log("pieceToMove", pieceToMove);
      console.log("newPosition", newPosition);
      newPosition.isKing = pieceToMove.isKing;
      if (newPosition.isKing) {
        console.log("move: already a king"); // DEBUG print.
      }
      return;
    }
  });

  if (find) {
    moveThePiece(newPosition);
  }

  else {

    // Clear the board from the current piece optional moves
    buildBoard();
  }
}

/**
 * This function searching for optional regulars moce
 * (UP-LEFT, UP-RIGHT, DOWN-LEFT, DOWN-RIGHT)
 * @param {Piece} piece the current player pieace
 * @returns {Array} A global possibleNewPositions list 
 */
function findPossibleNewPosition(piece, colorThePosition = true) {

  // reset possible new positions from other typings
  possibleNewPositions = [];

  // UP LEFT
  if ((currentPlayer === WHITE_PLAYER || piece.isKing) && piece.row >= 1 && piece.column >= 1 && board[piece.row - 1][piece.column - 1].player === EMPTY_CELL) {
    readyToMove = piece;
    displayPossiblePosition(piece, BLACK_PLAYER, -1, colorThePosition);
  }

  // UP RIGHT
  if ((currentPlayer === WHITE_PLAYER || piece.isKing) && piece.row >= 1 && piece.column + 1 < BOARD_SIZE && board[piece.row - 1][piece.column + 1].player === EMPTY_CELL) {
    readyToMove = piece;
    displayPossiblePosition(piece, BLACK_PLAYER, 1, colorThePosition);
  }

  // DOWN LEFT
  if ((currentPlayer === BLACK_PLAYER || piece.isKing) && piece.row + 1 < BOARD_SIZE && piece.column >= 1 && board[piece.row + 1][piece.column - 1].player === EMPTY_CELL) {
    readyToMove = piece;
    displayPossiblePosition(piece, WHITE_PLAYER, -1, colorThePosition);
  }

  // DOWN RIGHT
  if ((currentPlayer === BLACK_PLAYER || piece.isKing) && piece.row + 1 < BOARD_SIZE && piece.column + 1 < BOARD_SIZE && board[piece.row + 1][piece.column + 1].player === EMPTY_CELL) {
    readyToMove = piece;
    displayPossiblePosition(piece, WHITE_PLAYER, 1, colorThePosition);
  }

  console.log(`found optional new positions:`);
  printPiecesArray(possibleNewPositions);
  return possibleNewPositions.length > 0;
}


function moveThePiece(newPosition) {

  // if the current piece can move on, edit the board and rebuild
  board[newPosition.row][newPosition.column].player = currentPlayer;
  board[readyToMove.row][readyToMove.column].player = EMPTY_CELL;

  // Check if `newPosition` is a king now.
  if ((currentPlayer === WHITE_PLAYER && newPosition.row === 0) ||
    (currentPlayer === BLACK_PLAYER && newPosition.row === (BOARD_SIZE - 1))) {
    newPosition.isKing = true;
    console.log("move: new king"); // DEBUG print.
  }

  // Update `newPosition.isKing` in board.
  board[newPosition.row][newPosition.column].isKing = newPosition.isKing;

  // init value
  readyToMove = null;
  possibleNewPositions = [];
  eatablePiecesPosititions = []
  currentPlayer = reversePlayer(currentPlayer)
  displayCurrentPlayer();
  buildBoard();
}