/**
 * This function is called if eatablePiecesPosititions list size is bigger than 0
 * If the clickedPosition is eaual to one of the eatable new positions
 * The function call to eatThePiece() function and eat the opponnet piece
 * @param {Piece} clickedPosition cell event
 */
function eatPiece(clickedPosition) {
  let pieceCanEat = false;
  let newPosition = null;
  let eatenPiece = null;

  eatablePiecesPosititions.forEach((element) => {
    if (element.newPosition.compare(clickedPosition)) {
      pieceCanEat = true;
      moveCounter[currentPlayer]++;
      newPosition = element.newPosition;
      eatenPiece = element.eatablePiece;
      return;
    }
  });

  if (pieceCanEat) {
    eatThePiece(newPosition, eatenPiece);
  }
  else {

    // Clear the board from the current piece optional moves
    buildBoard();
  }
}


/**
 * This function searching for eatable opponent pieces in four directions
 * (UP-LEFT, UP-RIGHT, DOWN-LEFT, DOWN-RIGHT)
 * @param {Piece} clickedPiece the current player pieace
 * @returns {Array} A global eatablePiecesPosititions list 
 */
function findEatablePiece(clickedPiece, colorThePosition = true) {
  const opponentPlayer = reversePlayer(currentPlayer);

  // reset eatable pieces from opponnent pieces that can be eaten by other piece 
  eatablePiecesPosititions = [];
  let found = false;
  let newPosition, eatablePiece;
  const row = clickedPiece.row;
  const column = clickedPiece.column;
  const isKing = clickedPiece.isKing;

  // UP LEFT
  if ((currentPlayer === WHITE_PLAYER || isKing) && row >= 2 && column > 1 && board[row - 1][column - 1].player === opponentPlayer && board[row - 2][column - 2].player === EMPTY_CELL) {
    found = true;
    newPosition = new Piece(row - 2, column - 2, isKing, currentPlayer);
    eatablePiece = new Piece(row - 1, column - 1);
    addEatablePosition(clickedPiece, newPosition, eatablePiece, colorThePosition)
  }

  // UP RIGHT
  if ((currentPlayer === WHITE_PLAYER || isKing) && row >= 2 && column + 2 < BOARD_SIZE && board[row - 1][column + 1].player === opponentPlayer && board[row - 2][column + 2].player === EMPTY_CELL) {
    found = true;
    newPosition = new Piece(row - 2, column + 2, isKing, currentPlayer);
    eatablePiece = new Piece(row - 1, column + 1)
    addEatablePosition(clickedPiece, newPosition, eatablePiece, colorThePosition);
  }

  // DOWN LEFT
  if ((currentPlayer === BLACK_PLAYER || isKing) && row + 2 < BOARD_SIZE && column >= 2 && board[row + 1][column - 1].player === opponentPlayer && board[row + 2][column - 2].player === EMPTY_CELL) {
    found = true;
    newPosition = new Piece(row + 2, column - 2, isKing, currentPlayer);
    eatablePiece = new Piece(row + 1, column - 1);
    addEatablePosition(clickedPiece, newPosition, eatablePiece, colorThePosition);
  }

  // DOWN RIGHT
  if ((currentPlayer === BLACK_PLAYER || isKing) && row + 2 < BOARD_SIZE && column + 2 < BOARD_SIZE && board[row + 1][column + 1].player === opponentPlayer && board[row + 2][column + 2].player === EMPTY_CELL) {
    found = true;
    newPosition = new Piece(row + 2, column + 2, isKing, currentPlayer);
    eatablePiece = new Piece(row + 1, column + 1);
    addEatablePosition(clickedPiece, newPosition, eatablePiece, colorThePosition);
  }

  console.log(`found eatable pieces:`);
  printEatablePositions(eatablePiecesPosititions);
  return found;
}


/**
 * This function adding an eatable optional move,
 * to eatable move options list
 * @param {Piece} clickedPiece the current player pieace
 * @param {Piece} newPosition the new position of the current player piece,
 *                            if he choose to eat the opponnet piece
 * @param {Piece} eatablePiece the opponnet eatable piece
 */
function addEatablePosition(clickedPiece, newPosition, eatablePiece, colorThePosition = true) {
  readyToMove = clickedPiece;
  displayPossiblePosition(newPosition, 0, 0, colorThePosition);
  // save the new position and the opponent's piece position
  eatablePiecesPosititions.push({
    newPosition: newPosition,
    eatablePiece: eatablePiece,
  });
}

function eatThePiece(newPosition, eatenPiece) {
  board[newPosition.row][newPosition.column].player = currentPlayer;
  board[readyToMove.row][readyToMove.column].player = EMPTY_CELL;
  board[eatenPiece.row][eatenPiece.column].player = EMPTY_CELL;

  // Check if should be a new king now.
  if ((currentPlayer === WHITE_PLAYER && newPosition.row === 0 && newPosition.player === WHITE_PLAYER) ||
    (currentPlayer === BLACK_PLAYER && newPosition.row === (BOARD_SIZE - 1) && newPosition.player === BLACK_PLAYER)) {
    board[newPosition.row][newPosition.column].isKing = true;
    console.log("eat: new king"); // DEBUG print.
  }

  // Check if the piece was already a king.
  if (newPosition.isKing) {
    board[newPosition.row][newPosition.column].isKing = true;
    console.log("eat: already a king"); // DEBUG print.
  }

  // Reinit ready to move value
  readyToMove = null;
  eatablePiecesPosititions = [];
  possibleNewPositions = [];

  /*
   * Check if there are possibility to capture other piece.
   * In case the current player isn't able to eat any more, then switch players.
   * Else, the current player is able to eat more - so give him another turn.
   */
  if (!findEatablePiece(newPosition)) {
    currentPlayer = reversePlayer(currentPlayer);
    displayCurrentPlayer();
  }

  buildBoard();
}