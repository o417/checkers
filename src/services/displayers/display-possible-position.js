/**
 * This function is called from one of the move functions
 * And its display the number of pieces each player has at to bottom of the game page
 */
function displayPossiblePosition(p, player = 0, direction = 0, colorThePosition = true) {
  attribute = parseInt(p.row + player) + "-" + parseInt(p.column + direction);

  position = document.querySelector("[data-position='" + attribute + "']");
  if (position) {
    if (colorThePosition) {
      console.log("COLORED"); // TODO: Debug print
      position.classList.add("readyToMove");
    }

    // save where it can move
    possibleNewPositions.push(new Piece(p.row + player, p.column + direction));
  }
}