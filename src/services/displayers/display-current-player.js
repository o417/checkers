/**
 * This function is called after the current player finished his move
 * And its display the next player at the top of the game page
 */
function displayCurrentPlayer() {

  // Display player color.
  var container = document.getElementById("next-player");
  if (container.classList.contains("whitePiece")) {
    container.setAttribute("class", "occupied blackPiece");
  } else {
    container.setAttribute("class", "occupied whitePiece");
  }

  displayCurrentPlayerName();
}

function displayCurrentPlayerName() {

  // Display player name.
  const playerNameDiv = document.getElementById("next-player-name");
  playerNameDiv.innerText = `${playersNames[currentPlayer]}`;
}