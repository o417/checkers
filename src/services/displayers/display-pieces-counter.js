/**
 * This function is called at the buildBoard() function
 * And its display the number of pieces each player has at to bottom of the game page
 */
function displayPiecesCounter(black, white) {
  var blackContainer = document.getElementById("black-player-count-pieces");
  var whiteContainer = document.getElementById("white-player-count-pieces");
  blackContainer.innerHTML = black;
  whiteContainer.innerHTML = white;
}