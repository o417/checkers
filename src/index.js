const modal = document.getElementById("easyModal");
let game = document.getElementById("game");

class Piece {
  constructor(row, column, isKing = false, player = EMPTY_CELL) {
    this.row = row;
    this.column = column;
    this.isKing = isKing;
    this.player = player;
  }

  toString() {
    return JSON.stringify(this, null, '\t');
  }

  compare(piece) {
    return piece.row === this.row && piece.column === this.column;
  }
}

let currentPlayer = WHITE_PLAYER;
let possibleNewPositions;
let eatablePiecesPosititions;
let moveCounter;
let board;
function runGame() {
  possibleNewPositions = [];
  eatablePiecesPosititions = [];
  moveCounter = {
    [WHITE_PLAYER]: 1,
    [BLACK_PLAYER]: 1,
  };
  board = buildBoardMatrix(BOARD_SIZE, NUM_PLAYER_PIECES_ROWS);

  displayCurrentPlayerName();
  buildBoard();
}

runGame();
