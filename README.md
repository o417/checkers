# Checkers Game

This project implements a Checkers game between two players.

## Main Functionallities

- Initial form that inputs the players' names.
- The Checkers board.
- Display the name of the player to make a turn.
- Display how many pieces are left for each player.
- Display a modal that announces the winner when the game ends, and the reason
for his win, with the option to restart the game.
- This Checkers is following the "American Checkers" rules.
    - A king can move in four ways, but can make a single step at a time.
- Once there is an option to eat, the game forces you to eat.
    - Once there is an option to eat, the game shows you where are the possible
    eating paths, and you must choose one of them.
- The game allows double eating.
    - If you ate the opponent, and after your turn it seems that the same piece
    you ate with can eat again - you will get another turn to do the eating.
    This is how you can make combos of eating.
- The user cannot make an illegal move. The UI displays and allows the user to
make only legal moves.
